<?php
$args = array( 
  'current' => max( 1, get_query_var( 'paged' ) ),  
  'prev_next' => false,
  'total' => $wp_query->max_num_pages,
  'type' => 'array'
);
?>
<?php get_header(); ?>
      <ul class="p-latest-news">
      <?php 
      if ( have_posts() ) :
        while ( have_posts() ) :
          the_post();
      ?>
        <li class="p-latest-news__item p-article05">
          <a href="<?php the_permalink(); ?>" class="p-hover-effect--<?php echo esc_attr( $options['hover_type'] ); ?>">
            <div class="p-article05__img">
              <?php 
              if ( has_post_thumbnail() ) {
                the_post_thumbnail( 'size2' );
              } else {
                echo '<img src="' . get_template_directory_uri() . '/assets/images/no-image-info.jpg" alt="">' . "\n";
              }
              ?>
            </div>
            <div class="p-article05__content">
              <?php if ( $options['news_show_date'] ) : ?>
              <time class="p-article05__date" datetime="<?php the_time( 'Y-m-d' ); ?>"><?php the_time( 'Y.m.d' ); ?></time>
              <?php endif; ?>                  
              <h3 class="p-article05__title"><?php echo is_mobile() ? wp_trim_words( get_the_title(), 25, '...' ) : wp_trim_words( get_the_title(), 35, '...' ); ?></h3> 
            </div>
          </a>
        </li>
        <?php
          endwhile;
          wp_reset_postdata();
        else :
          echo '<li>' . __( 'There is no registered post.', 'tcd-w' ) . '</li>' . "\n";
        endif;
        ?>
      </ul>
      <?php if ( paginate_links( $args ) ) : ?>
      <ul class="p-pager">
        <?php foreach ( paginate_links( $args ) as $link ) : ?>
        <li class="p-pager__item"><?php echo $link; ?></li>
        <?php endforeach; ?>
      </ul>
      <?php endif; ?>
    </div>
    <?php get_sidebar(); ?>
  </div>
</div>
</main>
<?php get_footer(); ?>
