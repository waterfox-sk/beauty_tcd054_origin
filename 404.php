<?php get_header(); ?>
		<div class="p-entry__body">
			<p><strong>大変申し訳ございませんが、<br>お探しのページは見つかりませんでした。</strong></p>
			<p class="well">以下の理由が考えられます。<br>・一時的にアクセスが出来なくなっている。<br>・すでに削除されて無効になっている。<br>・URLの入力やリンク先が間違っている、変更されている。</p>
			<p>恐れ入りますが、<a href="<?php echo esc_url( home_url( '/' ) ); ?>">TOP</a>ページに戻っていただくか、上部メニューやサイト内検索からページをお探しください。</p>
		</div>
    </div>
    <?php get_sidebar(); ?>
  </div>
</div>
</main>
<?php get_footer(); ?>
